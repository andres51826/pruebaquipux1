package pruebaQuipux;

import java.util.Scanner;

public class Main {

	private static Scanner entradaEscaner;

	public static void main(String[] args) {
	     entradaEscaner = new Scanner(System.in);
	     System.out.println("Digite un numero: ");
	     
	      try {
	    	  int numero = entradaEscaner.nextInt();
		      if(numero % 2 == 0) {
		    	  if(numero>=2 && numero <=5) {
		    		  System.out.println("No Quipux");
		    	  }else if(numero>=6 && numero <=20) {
		    		  System.out.println("Quipux");
		    	  }else {
		    		  System.out.println("No Quipux");
		    	  }
		      }else {
		    	  System.out.println("Quipux");
		      }
	      }catch(Exception e){
	    	  System.out.println("Solo se acepta numeros enteros: "+e.toString());
	      }

	}

}
